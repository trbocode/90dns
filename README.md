# 90dns

Nintendo Switch DNS setup with BIND9 and nginx for blocking all Nintendo servers.

This is mostly for use with ftpd/sys-ftpd, hb-appstore and various updaters etc.

As it blocks Nintendo servers, eshop, updates, telemetry, social feature and online play will not work. People also reported that Fortnite doesn't work.

This is a LavaTech service.

[LavaTech Discord](https://discord.gg/urgYG9S)

[LavaTech Patreon](https://patreon.com/aveao)

## Prebuilt setup

I have a setup of this on `163.172.141.219`, feel free to use it yourself (see "Usage on Switch").

Also there's a copy of it hosted on `45.248.48.62` by adran. Thanks adran!

## Usage on Switch

- Optional: Do tests on your PC first to see if your ISP hijacks DNS (I've seen 3 reports of this so far)
- Go to System Settings on main menu
- Go to Internet tab
- Open Internet Settings
- Open Manual Setup and set up your network name, SSID and Security
- Set DNS Settings to manual, and set DNS options to either the IPs provided above (set both primary and secondary) or your own server's IP (see below for setup instructions)
- Save and then connect to wifi

If you did everything correctly, you should be on 90DNS.

### Testing DNS connectivity

On your PC you can set your DNS to one of the IPs provided above and try pinging the IPs (`ping example.nintendo.com`). Also on linux (and also mac?), you can use dig or drill with `drill example.nintendo.com`/`dig example.nintendo.com`. If you see 127.0.0.1, then you're connected properly.

On your Switch, you can try to go to eshop. If you get 2811-1007 error then you're successfully using 90DNS, if you successfully connect to eshop then you're not using 90DNS and if you get 2124-4007 then you're both not using 90DNS and are console banned (rip).

---

## Self-hosting Guide

**You don't have to self host.** You can simply use one of the IPs above and it'll have the same functionality. This is mostly for people who want to block more/less addresses or for people who have issues accessing dns servers outside of their networks due to their ISPs.

[The selfhosting guide was moved to SELFHOST.md](SELFHOST.md)

## Disclaimer

This is not guaranteed to prevent bans. It should, but I won't guarantee that.

This currently doesn't have a way of accepting reports from console, and I don't know if I'll ever add that as this is intended to be simple and plug-and-play, and that'd require patches on the device. Just make sure that you wipe reports before leaving this DNS (keep in mind that that might also mean a ban due to local logs not matching server ones).

If anyone's willing to help me with adding support for that (even if it means patches on switch), contact me.

